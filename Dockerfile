ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG url=HeinleinSupport/olefy/master/olefy.py
ARG APKVER

RUN adduser -D olefy --gecos "olefy scanner" 2>/dev/null \
&& apk update \
&& apk upgrade --available --no-cache \
&& apk add -u --no-cache openssl gzip py3-defusedxml py3-oletools py3-magic patch

WORKDIR /home/olefy/
COPY --chmod=644 --chown=olefy:olefy profile profile

WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh container-scripts/health-nc.sh entrypoint.sh ./

SHELL [ "/bin/ash", "-o", "pipefail", "-c" ]

RUN wget -q -S https://raw.githubusercontent.com/$url 2>&1 | grep "ETag:" \
| sed -e s+\"++g -e 's+.*ETag:\ ++' > /etc/githash \
&& chmod 755 olefy.py \
&& wget -O olefy.patch https://github.com/HeinleinSupport/olefy/pull/20/commits/90fc27ccc82494988990e4b53a992c6e4c065caa.patch \
&& patch -p1 -i olefy.patch \
&& rm olefy.patch

CMD [ "entrypoint.sh" ]
EXPOSE 10050

HEALTHCHECK --start-period=60s CMD health-nc.sh PING 10050 PONG || exit 1
